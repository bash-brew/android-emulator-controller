#!/bin/bash

# Global Variables
USER=$(whoami)
EMULATOR_CMD="$HOME/Library/Android/sdk/emulator/emulator"
AVD_LIST_FILE="avds.txt"
DELAY=.5

# Main is called in interactive loop before EOF
main() {
    print_emulator_lists
    print_options
    exec_option
}

# Print emulator lists
print_emulator_lists() {
    echo "All EMULATORS:"
    echo
    print_all_emulators

    echo "STARTUP LIST:"
    echo
    print_startup_list
}

# Print all emulators
print_all_emulators() {
    avd_list=$("$EMULATOR_CMD" -list-avds | grep -v INFO )
    if [ -n "$avd_list" ]; then
        echo "$avd_list"
    else
        echo "Add an emulator to this list using the Android Studio Virtual Device Manager."
    fi
    echo
}

# Print the startup list
print_startup_list() {
    if [ -s "$AVD_LIST_FILE" ]; then
        cat "$AVD_LIST_FILE"
    else
        echo "The AVD list is empty. Use option (a) to add to this list."
    fi
    echo
}

# Print options
print_options() {
    cat <<EOF
OPTIONS:

    a   -   Add emulator to startup list.
    r   -   Remove emulator from startup list.
    c   -   Clear startup list.
    s   -   Start Android emulators.
    w   -   Wipe all emulator data.
    q   -   Quit all running emulators.
ctrl+c  -   Close Android Emulator Controller.
EOF
echo
}

# Get user input and execute corresponding actions
exec_option() {
    read -p "Enter option (a/r/c/s/q): " INPUT
    case $INPUT in
        a)
            add_avd
            ;;
        r)
            remove_avd
            ;;
        c)
            remove_avd_list_file
            ;;
        s)
            start_avds
            ;;
        w)
            wipe_avds
            ;;
        q)
            stop_avds
            ;;
        *)
            echo "Option not recognized."
            ;;
    esac
}

# Add an AVD to the startup list
add_avd() {
    clear
    print_emulator_lists
    read -p "Enter emulator name to add: " NAME
    if [ -n "$NAME" ]; then
        print_all_emulators | grep "$NAME" >> "$AVD_LIST_FILE"
        echo "$NAME added to list."
    else
        echo "Invalid emulator name. Please enter an emulator name."
    fi
    echo
}

# Remove an AVD from the startup list
remove_avd() {
    clear
    print_emulator_lists
    read -p "Enter emulator name to remove: " NAME
    if [ -n "$NAME" ]; then
        sed -i.bak "/$NAME/d" "$AVD_LIST_FILE"
        echo "'$NAME' removed from the list."
    else
        echo "Invalid emulator name. Please enter a valid emulator name."
    fi
    echo
}

# Removes AVD list files
remove_avd_list_file() {
    clear
    if [ -f "$AVD_LIST_FILE" ]; then
        rm "$AVD_LIST_FILE"
        rm "$AVD_LIST_FILE.bak"
        echo "Emulator list file removed."
    else
        echo "Emulator list file doesn't exist."
    fi
    echo
}

# Start AVD emulators
start_avds() {
    clear
    echo "Starting emulators..."
    print_startup_list
    echo
    while IFS= read -r NAME; do
        if [ -n "$NAME" ]; then
            "$EMULATOR_CMD" -avd "$NAME" > /dev/null 2>&1 &
            echo "Starting $NAME."
            print_dots
        fi
    done < "$AVD_LIST_FILE"
    sleep "$DELAY"
}

# Wipe AVD emulators
wipe_avds() {
    clear
    echo "Starting emulators..."
    print_startup_list
    echo
    while IFS= read -r NAME; do
        if [ -n "$NAME" ]; then
            "$EMULATOR_CMD" -avd "$NAME" -wipe-data > /dev/null 2>&1 &
            echo "Starting $NAME."
            print_dots
        fi
    done < "$AVD_LIST_FILE"
    sleep 30
    stop_avds
}

# Stop AVD emulators
stop_avds() {
    PIDS=$(pgrep -f "emulator")
    clear
    echo "Stopping all running emulators..."
    echo
    if [ -n "$PIDS" ]; then
        kill $PIDS
        echo "All emulators stopped."
        print_dots
    else
        echo "No running emulators found."
    fi
    sleep "$DELAY"
    echo
}

# Print dots
print_dots() {
    DELAY=1
    printf ".........."
    sleep "$DELAY"
    printf ".........."
    sleep "$DELAY"
    printf ".........."
    sleep "$DELAY"
    printf "..........\n"
}

# Begin script execution
trap 'stop_avds; exit' INT      # Trap interrupt signal (Ctrl+C) to stop AVDs gracefully
while true ; do                 # Interactive loop
    clear
    main                        # Main execution
    sleep "$DELAY"
done
