# Android Emulator Controller

## About

This Bash script provides an interactive way to manage starting and stopping a list of Android Studio emulators on MacOS.

## Prerequisites

- MacOS
- Android Studio
- Devices configured in Virtual Device Manager

## Instalation

1.  Change to the directory that you would like the script to run from.
2.  Paste the folowing command into a terminal and press enter to run:
```bash
curl -sO https://gitlab.com/bash-brew/android-emulator-controller/-/raw/main/emu_ctrl.sh ; chmod +x emu_ctrl.sh ; ./emu_ctrl.sh
```
3.  Answer on-screen prompts.
4.  Press ctrl+c to quit.

## Usage

```bash
OPTIONS:

    a   -   Add emulator to startup list.
    r   -   Remove emulator from startup list.
    c   -   Clear startup list.
    s   -   Start Android emulators.
    w   -   Wipe All emulator data.
    q   -   Quit all running emulators.
ctrl+c  -   Close Android Emulator Controller.
```
To use script again:
```bash
./emu_ctrl.sh
```
